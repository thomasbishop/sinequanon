# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](https://semver.org/).

>Interested in adding a feature? Please see the [Project Board](https://github.com/thomasabishop/sinequanon/projects) for details of intended updates.

## [1.0.0] 2019-09-21 

### Added
* Development server
* CSS building
* SCSS compilation with pre-built Sass directories
* JavaScript/CSS minification/compression
* PostCSS:
    * Automated vendor prefixing
    * Automatic purge of unused CSS rules
    * Normalise.scss
* Lossless image compression for file types:
    * PNG
    * JPEG
    * GIF
    * SVG
* Partitioned Webpack config 
### Changed 

### Fixed 